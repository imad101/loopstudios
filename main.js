import "./style.css";

const toggleButton = document.querySelector("[data-toggle-btn]");
const toggleMenu = document.querySelector("[ data-toggle-nav]");
const toggleImg = document.querySelector("[ data-toggle-img]");

toggleButton.addEventListener("click", () => {
  let isOpen = toggleMenu.getAttribute("data-isOpen");
  console.log(isOpen);
  if (isOpen === "false") {
    toggleMenu.classList.remove("-left-full");
    toggleMenu.classList.add("left-0");
    toggleMenu.setAttribute("data-isOpen", "true");
    toggleImg.setAttribute("src", "./images/icon-close.svg");
    toggleImg.setAttribute("alt", "close menu");
  } else {
    toggleMenu.classList.remove("left-0");
    toggleMenu.classList.add("-left-full");
    toggleMenu.setAttribute("data-isOpen", "false");
    toggleImg.setAttribute("src", "./images/icon-hamburger.svg");
    toggleImg.setAttribute("alt", "open menu");
  }
});
