module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    fontFamily: {
      body: ["Alata", " sans-serif"],
      display: ["Josefin Sans", "sans-serif"],
    },

    extend: {
      colors: {
        gray: "hsl(0,0%,55%)",
        dark: "hsl(0,0%,41%)",
      },
    },
  },
  plugins: [],
};
